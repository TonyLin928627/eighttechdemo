package com.example.tonylin.a8techpraticalassinment.Uitls;


import android.text.SpannableStringBuilder;

import junit.framework.Assert;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

/**
 * Created by tonylin on 15/07/16.
 */
public class CalculatorTest {
    private Calculator calculator;
    @Before
    public void setUp(){
        this.calculator = Calculator.getInstance();
    }

    @After
    public void tearDown(){
        this.calculator.setFirstNumber(null);
        this.calculator.setOperator(null);
        this.calculator.setSecondNumber(null);
    }

    @Test
    public void testGetInstance() throws Exception {
        Assert.assertNotNull(Calculator.getInstance());
    }

    @Test
    public void testParseTextWithIllegalStrings() throws Exception {

        //illegal strings
        final String[] illegalTestStrings = new String[]{
                "*",
                "-",
                "+",
                "0",
                "123*0",
                "124-134*",
                "124-0321",
        };

        for (String inputStr : illegalTestStrings) {
            String outputStr = this.calculator.parseText(inputStr);

            Assert.assertNull("inputStr=" + inputStr, outputStr);
        }
    }

    @Test
    public void testParseTextWithLegalStrings() throws Exception {
        //legal strings
        final String[] legalTestStrings = new String[]{
                "123",
                "345*",
                "123*-",
                "124-134",
        };

        for (String inputStr : legalTestStrings) {
            String outputStr = this.calculator.parseText(inputStr);

            Assert.assertNotNull("inputStr="+inputStr, outputStr);
        }
    }

    @Test
    public void testDealWithPattern1() throws Exception {
        final String inputStr = "111";

        Assert.assertNull("firstNumber is "+this.calculator.getFirstNumber(), this.calculator.getFirstNumber());
        Assert.assertNull(this.calculator.getSecondNumber());
        Assert.assertNull(this.calculator.getOperator());

        final String outputStr = this.calculator.dealWithPattern1(inputStr);
        //the output should be the same as input
        Assert.assertEquals("outputStr is "+outputStr, outputStr, inputStr);

        //the firstNumber should has been set to inputStr
        Assert.assertEquals("firstNumber="+this.calculator.getFirstNumber(), this.calculator.getFirstNumber(), inputStr);
        Assert.assertNull(this.calculator.getSecondNumber());
        Assert.assertNull(this.calculator.getOperator());
    }

    @Test
    public void testDealWithPattern2_1() throws Exception {

        this.testDealWithPattern2("+");
    }

    @Test
    public void testDealWithPattern2_2() throws Exception {

        this.testDealWithPattern2("-");
    }

    @Test
    public void testDealWithPattern2_3() throws Exception {

        this.testDealWithPattern2("*");
    }

    private void testDealWithPattern2(String operator){
        final String firstNumber = "222";
        final String inputStr = firstNumber+operator;

        Assert.assertNull("firstNumber is "+this.calculator.getFirstNumber(), this.calculator.getFirstNumber());
        Assert.assertNull(this.calculator.getSecondNumber());
        Assert.assertNull("operator is "+this.calculator.getOperator(), this.calculator.getOperator());

        String outputStr = this.calculator.dealWithPattern2(inputStr);
        //the output should be the same as input
        Assert.assertEquals("outputStr is "+outputStr, outputStr, inputStr);

        //the firstNumber should has been set to inputStr
        Assert.assertEquals("firstNumber="+this.calculator.getFirstNumber(), this.calculator.getFirstNumber(), firstNumber);
        Assert.assertNull(this.calculator.getSecondNumber());
        Assert.assertEquals("operator is "+this.calculator.getOperator(), this.calculator.getOperator(), operator);
    }

    @Test
    public void testDealWithPattern3() throws Exception {

        final String firstNumber = "333";
        final String oldOperator = "+";
        final String newOperator = "*";

        this.calculator.setFirstNumber(firstNumber);
        this.calculator.setOperator(oldOperator);

        Assert.assertEquals(this.calculator.getFirstNumber(), firstNumber);
        Assert.assertEquals(this.calculator.getOperator(), oldOperator);

        final String inputStr = firstNumber+oldOperator+newOperator;
        final String outputStr = this.calculator.dealWithPattern3(inputStr);

        //output should be 333*
        Assert.assertEquals(outputStr, firstNumber+newOperator);

        Assert.assertEquals(this.calculator.getFirstNumber(), firstNumber);

        Assert.assertEquals(this.calculator.getOperator(), newOperator);


    }

    @Test
    public void testDealWithPattern4_1() throws Exception {
        this.testDealWithPattern4("+");
    }

    @Test
    public void testDealWithPattern4_2() throws Exception {
        this.testDealWithPattern4("-");
    }

    @Test
    public void testDealWithPattern4_3() throws Exception {
        this.testDealWithPattern4("*");
    }

    public void testDealWithPattern4(String operator) throws Exception {
        final String firstNumber = "441";
        final String secondNumber = "442";
        final String inputStr = firstNumber+operator+secondNumber;


        Assert.assertNull("firstNumber is "+this.calculator.getFirstNumber(), this.calculator.getFirstNumber());
        Assert.assertNull("secondNumber is "+this.calculator.getSecondNumber(), this.calculator.getSecondNumber());
        Assert.assertNull("operator is "+this.calculator.getOperator(), this.calculator.getOperator());

        String outputStr = this.calculator.dealWithPattern4(inputStr);
        //the output should be the same as input
        Assert.assertEquals("outputStr is "+outputStr, outputStr, inputStr);

        //the firstNumber should has been set to inputStr
        Assert.assertEquals("firstNumber="+this.calculator.getFirstNumber(), this.calculator.getFirstNumber(), firstNumber);
        Assert.assertEquals("secondNumber="+this.calculator.getSecondNumber(), this.calculator.getSecondNumber(), secondNumber);
        Assert.assertEquals("operator is "+this.calculator.getOperator(), this.calculator.getOperator(), operator);
    }

}