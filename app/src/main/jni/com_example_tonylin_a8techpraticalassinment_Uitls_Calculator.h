/* DO NOT EDIT THIS FILE - it is machine generated */
#include <jni.h>
/* Header for class com_example_tonylin_a8techpraticalassinment_Uitls_Calculator */

#ifndef _Included_com_example_tonylin_a8techpraticalassinment_Uitls_Calculator
#define _Included_com_example_tonylin_a8techpraticalassinment_Uitls_Calculator
#ifdef __cplusplus
extern "C" {
#endif
/*
 * Class:     com_example_tonylin_a8techpraticalassinment_Uitls_Calculator
 * Method:    timeAndMod7
 * Signature: (II)I
 */
JNIEXPORT jint JNICALL Java_com_example_tonylin_a8techpraticalassinment_Uitls_Calculator_timeAndMod7
  (JNIEnv *, jobject, jint, jint);

/*
 * Class:     com_example_tonylin_a8techpraticalassinment_Uitls_Calculator
 * Method:    plusAndMod7
 * Signature: (II)I
 */
JNIEXPORT jint JNICALL Java_com_example_tonylin_a8techpraticalassinment_Uitls_Calculator_plusAndMod7
  (JNIEnv *, jobject, jint, jint);

/*
 * Class:     com_example_tonylin_a8techpraticalassinment_Uitls_Calculator
 * Method:    subAndMod7
 * Signature: (II)I
 */
JNIEXPORT jint JNICALL Java_com_example_tonylin_a8techpraticalassinment_Uitls_Calculator_subAndMod7
  (JNIEnv *, jobject, jint, jint);

#ifdef __cplusplus
}
#endif
#endif
