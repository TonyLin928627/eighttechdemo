#include "com_example_tonylin_a8techpraticalassinment_Uitls_Calculator.h"
#include <inttypes.h>
/*
 * Class:     com_example_tonylin_a8techpraticalassinment_Uitls_Calculator
 * Method:    timeAndMod7
 * Signature: (II)I
 */
JNIEXPORT jint JNICALL Java_com_example_tonylin_a8techpraticalassinment_Uitls_Calculator_timeAndMod7
  (JNIEnv *env, jobject obj, jint first_number, jint second_number){

        uint64_t temp = first_number*second_number;

        jint i = temp%7;

        return i;
  }

/*
 * Class:     com_example_tonylin_a8techpraticalassinment_Uitls_Calculator
 * Method:    plusAndMod7
 * Signature: (II)I
 */
JNIEXPORT jint JNICALL Java_com_example_tonylin_a8techpraticalassinment_Uitls_Calculator_plusAndMod7
  (JNIEnv *env, jobject obj, jint first_number, jint second_number){

        uint64_t temp = first_number+second_number;

        jint i = temp%7;

        return i;
  }

/*
 * Class:     com_example_tonylin_a8techpraticalassinment_Uitls_Calculator
 * Method:    subAndMod7
 * Signature: (II)I
 */
JNIEXPORT jint JNICALL Java_com_example_tonylin_a8techpraticalassinment_Uitls_Calculator_subAndMod7
  (JNIEnv *env, jobject obj, jint first_number, jint second_number){

        int64_t temp = first_number-second_number;

        jint i = temp%7;
        return i;
  }