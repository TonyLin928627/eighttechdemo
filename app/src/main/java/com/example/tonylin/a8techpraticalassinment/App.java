package com.example.tonylin.a8techpraticalassinment;

import android.app.Application;
import android.content.Context;
import android.os.Build;
import android.provider.Settings;
import android.util.DisplayMetrics;
import android.view.Display;
import android.view.WindowManager;

/**
 * Created by tonylin on 15/07/16.
 */
public class App extends Application {

    private static int screenWidth = 0;
    private static int screenHeight = 0;
    private static float density = 1;

    public static App instance;

    public static int getScreenWidth(){
        return screenWidth;
    }

    public static int getScreenHeight(){
        return screenHeight;
    }

    public static float getDensity(){
        return density;
    }

    public void readScreenDimensions() {

        WindowManager wm = (WindowManager) this.getSystemService(Context.WINDOW_SERVICE);
        Display display = wm.getDefaultDisplay();
        DisplayMetrics metrics = new DisplayMetrics();
        display.getMetrics(metrics);

        App.screenWidth = metrics.widthPixels;
        App.screenHeight = metrics.heightPixels;
        App.density = metrics.density;
    }

    @Override
    public void onCreate() {
        super.onCreate();

        App.instance = this;
//        this.readScreenDimensions();

    }
}
