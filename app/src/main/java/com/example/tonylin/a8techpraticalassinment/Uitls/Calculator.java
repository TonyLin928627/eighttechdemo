package com.example.tonylin.a8techpraticalassinment.Uitls;

import android.util.Log;

import java.util.HashSet;
import java.util.Set;
import java.util.regex.Pattern;

/**
 * Created by tonylin on 15/07/16.
 */
public class Calculator {

    /*
    Native methods
     */
    private native int timeAndMod7(int firstNumber, int secondNumber);
    private native int plusAndMod7(int firstNumber, int secondNumber);
    private native int subAndMod7(int firstNumber, int secondNumber);

    private static final Set<String> operatorSet = new HashSet<>();

    private final static Pattern p0 = Pattern.compile("^[0]{1}[1-6]*$"); //start with 0. e.g. 01
    private final static Pattern p1 = Pattern.compile("^[1-6]+[0-6]*$"); //firstNumber. e.g. 124
    private final static Pattern p2 = Pattern.compile("^[1-6]+[0-6]*[\\+,\\*,-]{1}$"); //firstNumber + operator. e.g. 124+
    private final static Pattern p3 = Pattern.compile("^[1-6]+[0-6]*[\\+,\\*,-]{2}$"); //firstNumber + operator + operator. e.g. 124+*
    private final static Pattern p4 = Pattern.compile("^[1-6]+[0-6]*[\\+,\\*,-]{1}[1-6]+[0-6]*$"); //firstNumber+operation+secondNumber e.g 124+334
    private final static Pattern p5 = Pattern.compile("^[-]{1}[1-6]*$"); //negative number e.g -123

    static {
        operatorSet.add("*");
        operatorSet.add("-");
        operatorSet.add("+");

        System.loadLibrary("Calculator");
    }

    //singleton
    private static Calculator ourInstance = new Calculator();
    private Calculator() {}

    private String operator;
    private String firstNumber, secondNumber;

    public static Calculator getInstance() {
        return ourInstance;
    }


    public String parseText(String str) {
        final CharSequence cs = str.trim().subSequence(0, str.length());

        if (cs.length() == 0) {
            return "";
        }

        if (p0.matcher(cs).matches()){
            return this.dealWithPattern0(cs);
        }


        if (p1.matcher(cs).matches()) {
            return this.dealWithPattern1(cs);
        }

        if (p2.matcher(cs).matches()) {
            return this.dealWithPattern2(cs);
        }

        if (p3.matcher(cs).matches()) {
            return this.dealWithPattern3(cs);
        }

        if (p4.matcher(cs).matches()) {
            return this.dealWithPattern4(cs);
        }

        if (p5.matcher(cs).matches()){
            return this.dealWithPattern5(cs);
        }

        this.firstNumber = this.secondNumber = this.operator = null;
        return null;
    }

    //remove the first char which is 0
    String dealWithPattern0(CharSequence cs) {
        return cs.subSequence(1, cs.length()).toString();
    }

    //set firstNumber only
    String dealWithPattern1(CharSequence cs) {

        this.firstNumber = cs.toString();
        this.secondNumber = this.operator = null;

        return cs.toString();
    }

    //set firstNumber and operator
    String dealWithPattern2(CharSequence cs) {
        this.firstNumber = cs.subSequence(0, cs.length() - 1) + "";
        this.operator = cs.charAt(cs.length() - 1) + "";

        return this.firstNumber + this.operator;
    }

    //set firstNumber and update operator
    String dealWithPattern3(CharSequence cs) {

        this.firstNumber = cs.subSequence(0, cs.length() - 2) + "";
        this.operator = cs.charAt(cs.length() - 1) + "";

        return this.firstNumber + this.operator;
    }

    //set firstNumber, operator and secondNumber
    String dealWithPattern4(CharSequence cs) {

        final String s = cs.toString();

        for (String operator : operatorSet) {
            final int index = s.indexOf(operator);
            if (index != -1) {
                this.firstNumber = s.subSequence(0, index).toString();
                this.operator = s.substring(index, index + 1);
                this.secondNumber = s.substring(index + 1);

                break;
            }
        }

        return s;
    }

    //replace negative number with its last digit
    String dealWithPattern5(CharSequence cs) {

        return cs.charAt(cs.length()-1)+"";

    }

    public String getOperator() {
        return operator;
    }

    void setOperator(String operator) {
        this.operator = operator;
    }

    public String getFirstNumber() {
        return firstNumber;
    }

    void setFirstNumber(String firstNumber) {
        this.firstNumber = firstNumber;
    }

    public String getSecondNumber() {
        return secondNumber;
    }

    void setSecondNumber(String secondNumber) {
        this.secondNumber = secondNumber;
    }

    public String calculate() {
        final String rtnStr;

        final int firstNumber, secondNumber;
        try {

            firstNumber = Integer.parseInt(this.firstNumber);
            secondNumber = Integer.parseInt(this.secondNumber);

//            firstNumber = Integer.MAX_VALUE;
//            secondNumber = Integer.MAX_VALUE;

        }catch (NumberFormatException e){
            Log.e("Calculator", e.getLocalizedMessage());
            return null;
        }

        switch (this.operator.charAt(0)) {
            case '*':
                rtnStr = String.valueOf(this.timeAndMod7(firstNumber, secondNumber));
                break;
            case '+':
                rtnStr = String.valueOf(this.plusAndMod7(firstNumber, secondNumber));
                break;
            case '-':
                rtnStr = String.valueOf(this.subAndMod7(firstNumber, secondNumber));
                break;
            default:
                rtnStr = null;
        }

        if (rtnStr!=null) {
            this.firstNumber = this.secondNumber = this.operator = null;
        }
        return rtnStr;
    }
}
