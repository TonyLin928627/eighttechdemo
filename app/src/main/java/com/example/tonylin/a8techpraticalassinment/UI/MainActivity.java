package com.example.tonylin.a8techpraticalassinment.UI;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.tonylin.a8techpraticalassinment.App;
import com.example.tonylin.a8techpraticalassinment.R;
import com.example.tonylin.a8techpraticalassinment.Uitls.Calculator;

public class MainActivity extends AppCompatActivity implements OnClickListener, TextWatcher, View.OnLongClickListener {

    private final static String TAG = "MainActivity";
    private EditText outputEt;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        App.instance.readScreenDimensions();
        this.initUIComponents();

    }

    //init the buttons and editText
    private void initUIComponents() {

        //init edit text and add text change listener
        this.outputEt = (EditText)this.findViewById(R.id.output_et);
        this.outputEt.addTextChangedListener(this);

        //init the buttons
        this.initButton(R.id.one_btn);
        this.initButton(R.id.two_btn);
        this.initButton(R.id.three_btn);
        this.initButton(R.id.four_btn);
        this.initButton(R.id.five_btn);
        this.initButton(R.id.six_btn);
        this.initButton(R.id.zero_btn);

        this.initButton(R.id.time_btn);
        this.initButton(R.id.sub_btn);
        this.initButton(R.id.plus_btn);
        this.initButton(R.id.equal_btn);
        this.initButton(R.id.del_btn);

        //add long click listener to del button so clear the text in edit text when long clicked
        this.initButton(R.id.del_btn).setOnLongClickListener(this);
    }

    /**
     * Instantiate button by the given resource id
     * 1> Set size
     * 2> Set onClickListener
     * @param resourceId
     * @return
     */
    private Button initButton(int resourceId) {

        //1. set onclick listener
        final Button rtnBtn = (Button)this.findViewById(resourceId);
        rtnBtn.setOnClickListener(this);

        //2. set the button's dimensions to fit screen size
        float leftAndRightMargin = (int)this.getResources().getDimension(R.dimen.activity_horizontal_margin)*2;

        final int rowWidth;
        if (App.getScreenHeight()>App.getScreenWidth()){
            rowWidth = 4;
        }else{
            rowWidth = 7;
        }

        final float gridViewWidth = App.getScreenWidth() - leftAndRightMargin;
        final int btnDimen = (int)Math.floor(gridViewWidth/rowWidth);

        rtnBtn.getLayoutParams().width = rtnBtn.getLayoutParams().height = (btnDimen);

        return rtnBtn;
    }

    @Override
    public void onClick(View v) {

        final Button btn = (Button)v;

        switch (v.getId()){
            case R.id.one_btn:
            case R.id.two_btn:
            case R.id.three_btn:
            case R.id.four_btn:
            case R.id.five_btn:
            case R.id.six_btn:
            case R.id.zero_btn:
            case R.id.time_btn:
            case R.id.sub_btn:
            case R.id.plus_btn:
                final CharSequence cs = btn.getText();
                String s = this.outputEt.getText().toString() + cs;
                this.outputEt.setText(s);
                break;
            case R.id.del_btn:
                Editable text = this.outputEt.getText();

                if (text.length()> 0) {
                    text.delete(text.length() - 1, text.length());
                }
                break;
            case R.id.equal_btn:
                final Calculator calculator= Calculator.getInstance();

                //compose text for toast
                final String resultDescription = "("+calculator.getFirstNumber()+calculator.getOperator()+calculator.getSecondNumber()+")%7=";
                //get the result text from calculator
                final String result = Calculator.getInstance().calculate();

                //text!=null  means calculate successfully
                if (result!=null) {
                    //pause the text change listener
                    this.isStopCallBack = true;
                    this.outputEt.setText(result);

                    //show toast message
                    Toast.makeText(this, resultDescription+result, Toast.LENGTH_LONG).show();
                }else {
                    Toast.makeText(this, "Invalid input", Toast.LENGTH_LONG).show();
                }
        }
    }

    //text change listener of edit text
    private String tempStr;
    private boolean isStopCallBack = false;
    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {
        this.tempStr = s.toString();
    }
    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {}

    @Override
    public void afterTextChanged(Editable s) {

        //ignore this change
        if (this.isStopCallBack){
            this.isStopCallBack = false;
            return;
        }

        this.isStopCallBack = true;
        String rtnStr = Calculator.getInstance().parseText(s.toString());
        this.outputEt.setText(rtnStr!=null ? rtnStr : this.tempStr);

    }

    @Override
    public boolean onLongClick(View v) {
        this.outputEt.getText().clear();
        return true;
    }
}
