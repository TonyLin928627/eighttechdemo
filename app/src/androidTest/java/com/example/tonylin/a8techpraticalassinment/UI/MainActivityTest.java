package com.example.tonylin.a8techpraticalassinment.UI;

import android.support.test.InstrumentationRegistry;
import android.support.test.espresso.ViewInteraction;
import android.support.test.espresso.matcher.ViewMatchers;
import android.support.test.runner.AndroidJUnit4;
import android.test.ActivityInstrumentationTestCase2;
import android.test.suitebuilder.annotation.LargeTest;

import com.example.tonylin.a8techpraticalassinment.R;
import com.example.tonylin.a8techpraticalassinment.UI.MainActivity;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.HashSet;
import java.util.Set;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.clearText;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.action.ViewActions.longClick;
import static android.support.test.espresso.action.ViewActions.typeText;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.matcher.ViewMatchers.withText;

/**
 * Created by tonylin on 15/07/16.
 */

@RunWith(AndroidJUnit4.class)
@LargeTest
public class MainActivityTest extends ActivityInstrumentationTestCase2<MainActivity> {

    private MainActivity mainActivity;

    private ViewInteraction outputEt;
    private ViewInteraction oneBtn, twoBtn, threeBtn, fourBtn, fiveBtn, sixBtn, zeroBtn,
                            timeBtn, subBtn, plusBtn, equalBtn, delBtn;

    public MainActivityTest() {
        super(MainActivity.class);
    }

    @Before
    public void setUp() {
        injectInstrumentation(InstrumentationRegistry.getInstrumentation());
        mainActivity = getActivity();

        this.outputEt = onView(ViewMatchers.withId(R.id.output_et));
        this.outputEt.perform(clearText());

        this.oneBtn = onView(withId(R.id.one_btn));
        this.twoBtn = onView(withId(R.id.two_btn));
        this.threeBtn = onView(withId(R.id.three_btn));
        this.fourBtn = onView(withId(R.id.four_btn));
        this.fiveBtn = onView(withId(R.id.five_btn));
        this.sixBtn = onView(withId(R.id.six_btn));
        this.zeroBtn = onView(withId(R.id.zero_btn));

        this.timeBtn = onView(withId(R.id.time_btn));
        this.subBtn = onView(withId(R.id.sub_btn));
        this.plusBtn = onView(withId(R.id.plus_btn));
        this.equalBtn = onView(withId(R.id.equal_btn));
        this.delBtn = onView(withId(R.id.del_btn));

    }

    @Test
    public void testUIComponentVisibility(){

        this.outputEt.check(matches(isDisplayed()));
        this.outputEt.check(matches(withText("")));

        this.oneBtn.check(matches(isDisplayed()));
        this.oneBtn.check(matches(withText("1")));

        this.twoBtn.check(matches(isDisplayed()));
        this.twoBtn.check(matches(withText("2")));

        this.threeBtn.check(matches(isDisplayed()));
        this.threeBtn.check(matches(withText("3")));

        this.fourBtn.check(matches(isDisplayed()));
        this.fourBtn.check(matches(withText("4")));

        this.fiveBtn.check(matches(isDisplayed()));
        this.fiveBtn.check(matches(withText("5")));

        this.sixBtn.check(matches(isDisplayed()));
        this.sixBtn.check(matches(withText("6")));

        this.zeroBtn.check(matches(isDisplayed()));
        this.zeroBtn.check(matches(withText("0")));

        this.timeBtn.check(matches(isDisplayed()));
        this.timeBtn.check(matches(withText("*")));

        this.subBtn.check(matches(isDisplayed()));
        this.subBtn.check(matches(withText("-")));

        this.plusBtn.check(matches(isDisplayed()));
        this.plusBtn.check(matches(withText("+")));

        this.equalBtn.check(matches(isDisplayed()));
        this.equalBtn.check(matches(withText("=")));

        this.delBtn.check(matches(isDisplayed()));
        this.delBtn.check(matches(withText("DEL")));


    }

    @Test
    public void testDigitBtns() {

        //test single type
        this.outputEt.perform(clearText());
        this.oneBtn.perform(click());
        this.outputEt.check(matches(withText("1")));

        this.outputEt.perform(clearText());
        this.twoBtn.perform(click());
        this.outputEt.check(matches(withText("2")));

        this.outputEt.perform(clearText());
        this.threeBtn.perform(click());
        this.outputEt.check(matches(withText("3")));

        this.outputEt.perform(clearText());
        this.fourBtn.perform(click());
        this.fourBtn.check(matches(withText("4")));

        this.outputEt.perform(clearText());
        this.fiveBtn.perform(click());
        this.outputEt.check(matches(withText("5")));

        this.outputEt.perform(clearText());
        this.sixBtn.perform(click());
        this.outputEt.check(matches(withText("6")));

        //0 cannot be the first char
        this.outputEt.perform(clearText());
        this.zeroBtn.perform(click());
        this.outputEt.check(matches(withText("")));

        //test concat
        this.outputEt.perform(clearText());
        this.oneBtn.perform(click());
        this.twoBtn.perform(click());
        this.threeBtn.perform(click());
        this.fourBtn.perform(click());
        this.fiveBtn.perform(click());
        this.sixBtn.perform(click());
        this.zeroBtn.perform(click());
        this.outputEt.check(matches(withText("1234560")));
    }

    @Test
    public void testDelBtn(){

        //test delete click, delete one by one from last char
        final CharSequence cs = "1234560";

        //enter 1234560
        this.outputEt.perform(clearText());
        this.oneBtn.perform(click());
        this.twoBtn.perform(click());
        this.threeBtn.perform(click());
        this.fourBtn.perform(click());
        this.fiveBtn.perform(click());
        this.sixBtn.perform(click());
        this.zeroBtn.perform(click());
        this.outputEt.check(matches(withText(cs.toString())));

        for (int i=0; i<cs.length(); i++){
            this.delBtn.perform(click());
            final String expectedStr = cs.subSequence(0, cs.length()-(i+1)).toString();
            this.outputEt.check(matches(withText(expectedStr)));
        }

        //test delete click while the text is blank
        this.outputEt.perform(clearText());
        this.delBtn.perform(click());
        this.outputEt.check(matches(withText("")));

        //test long click to clear text
        this.outputEt.perform(typeText("123"));
        this.delBtn.perform(longClick());
        this.outputEt.check(matches(withText("")));

        //test long click while the text is blank
        this.outputEt.perform(clearText());
        this.delBtn.perform(longClick());
        this.outputEt.check(matches(withText("")));
    }

    @Test
    public void testTimeBtn(){
        this.testOperators(this.timeBtn, '*');
    }

    @Test
    public void testPlusBtn(){
        this.testOperators(this.plusBtn, '+');
    }

    @Test
    public void testSubBtn(){
        this.testOperators(this.subBtn, '-');
    }

    private void testOperators(ViewInteraction operator, char operatorChar){

        //operator cannot be the first char
        this.delBtn.perform(longClick());
        operator.perform(click());
        this.outputEt.check(matches(withText("")));

        //enter "123"
        this.oneBtn.perform(click());
        this.twoBtn.perform(click());
        this.threeBtn.perform(click());
        //enter operator
        operator.perform(click());
        this.outputEt.check(matches(withText("123"+operatorChar)));

        //existing operator will be replaced

        this.timeBtn.perform(click());
        this.outputEt.check(matches(withText("123*")));

        this.subBtn.perform(click());
        this.outputEt.check(matches(withText("123-")));

        this.plusBtn.perform(click());
        this.outputEt.check(matches(withText("123+")));
    }

    @Test
    public void testEqualBtn(){

        //press while 1
        this.oneBtn.perform(click());
        this.outputEt.check(matches(withText("1")));
        this.equalBtn.perform(click());
        this.outputEt.check(matches(withText("1")));

        //press while 12
        this.twoBtn.perform(click());
        this.outputEt.check(matches(withText("12")));
        this.equalBtn.perform(click());
        this.outputEt.check(matches(withText("12")));

    }
}
