package com.example.tonylin.a8techpraticalassinment.Uitls;

import android.util.Log;

import junit.framework.Assert;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Created by tonylin on 16/07/16.
 */
public class CalculatorTest_jni {

    private Calculator calculator;

    @Before public void setUp(){
        this.calculator = Calculator.getInstance();
    }

    @Test
    public void testCalculate() throws Exception {
        final String[] operators = new String[]{"*", "-", "+"};

        for (int i=0; i<operators.length; i++) {
            for (int firstNumber = -1000; firstNumber <= 1000; firstNumber++) {
                for (int secondNumber = -1000; secondNumber <= 1000; secondNumber++) {
                    this.calculator.setFirstNumber(firstNumber + "");
                    this.calculator.setSecondNumber(secondNumber + "");
                    this.calculator.setOperator(operators[i]);

                    final int result = Integer.parseInt(this.calculator.calculate());
                    Log.i("CalculatorTest_jni", this.calculator.getFirstNumber()+this.calculator.getOperator()+this.calculator.getSecondNumber());

                    switch (this.calculator.getOperator()) {
                        case "*":
                            Assert.assertEquals(result, (firstNumber * secondNumber) % 7);
                            break;
                        case "-":
                            Assert.assertEquals(result, (firstNumber - secondNumber) % 7);
                            break;
                        case "+":
                            Assert.assertEquals(result, (firstNumber + secondNumber) % 7);
                            break;
                    }

                }
            }
        }
    }
}